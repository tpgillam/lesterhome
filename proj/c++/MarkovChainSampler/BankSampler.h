
#ifndef LESTER_BANKSAMPLER_H
#define LESTER_BANKSAMPLER_H

namespace Lester {

  template <class PointType> class Bank {
    class 
  public:
    
    std::vector<PointType>
  };
  
  template <class PointType> class BankSampler : public MetropolisSampler<PointType> {
  private:
    BankSampler(const Bank & bank); // Deliberately not implemented!
    
  public:
    
  protected:
    
  };

}

#endif
