
#include "SimpleGaussianBankSampler.h" // Because we want to define a SimpleGaussianBankSampler!
#include <iostream>                    // So that we can send some data to the screen!

/**
 *  Whatever class you use to hold the sampled points must be
 *  capable of supporting the operations of:
 *
 *    - Right-Multiplication by double:   point * 3.2
 *    - Addition:                         point1 + point2
 *    - Subtraction:                      point1 - point2
 *
 *  The following "class ExamplePoint" is an example of 
 *  such a class.
 */

class ExamplePoint {
 public:
  double x;
  double y;
  ExamplePoint(double x, double y) : x(x), y(y) {}
  const double & operator[](const unsigned int index) const {
    if (index==0) {
      return x;
    } else {
      return y;
    }
  }
  double & operator[](const unsigned int index) {
    if (index==0) {
      return x;
    } else {
      return y;
    }
  }
};


/** 
Here are some test functions
*/

/// An annulus or ring with a gaussian profile:
double logDistribution_RING(const ExamplePoint & p) {
  const double r  = (p.x*p.x + p.y*p.y);
  const double r0 = 1;
  const double width = 0.1;
  return -(r-r0)*(r-r0)/(2*width*width);
}

/// A distribution uniform on the unit square
double logDistribution_UNIT_SQUARE(const ExamplePoint & p) {
    if (p.x>1 || p.x<-1 || p.y>1 || p.y<-1) {
      throw Lester::LogarithmicTools::LogOfZero();
    } else {
      return 0;
    }
}


/**
 * Now we make a sampler that can sample the space of the 
 * above points.
 */

unsigned int distributionToUse=0;

class Example_SimpleGaussianBankSampler 
  : public Lester::SimpleGaussianBankSampler<ExamplePoint, 2> {
 public:
  // It is helpful to define the following shorthand:
  typedef Lester::SimpleGaussianBankSampler<ExamplePoint, 2>::Bank Bank;

  /**
   * We must define the function we wish to sample from.
   * We do this by implementing the following (previously
   * pure virtual) method:
   */
  double logTargetDistribution(const ExamplePoint & p) const {
    switch (distributionToUse) {
    case 0:
      return logDistribution_RING(p);
    case 1:
      return logDistribution_UNIT_SQUARE(p);
    default:
      return logDistribution_RING(p);
    }    
  }

  /**
   *  As we are a bank sampler, we must pass the relevant
   *  "bank" on to our parent class when we are constructed:
   */
  Example_SimpleGaussianBankSampler(const ExamplePoint & startPoint,
				    const Example_SimpleGaussianBankSampler::Bank & bank,
				    const ExamplePoint & widthsForGaussianProposals) :
    Lester::SimpleGaussianBankSampler<ExamplePoint, 2>(startPoint, bank, widthsForGaussianProposals) {
  }


};


int main(int n, char * args[]) {

  if (n!=2) {
    std::cerr << "Usage: " << args[0] << " N\nWhere N is 0 (for a ring distribution) or 1 (for unit square distribution)" << std::endl;
    return 1;
  }

  // Read into "distributionToUse" a number like "0" or "1" to determine the type of test distribution from which we will sample.
  std::istringstream s(args[1]);
  s >> distributionToUse;

  Example_SimpleGaussianBankSampler::Bank bank;

  bank.addBankPoint(ExamplePoint( 1    , 0    ));
  bank.addBankPoint(ExamplePoint( 0.707, 0.707));
  bank.addBankPoint(ExamplePoint( 1    , 1    ));
  bank.addBankPoint(ExamplePoint(-1    , 0    ));

  const ExamplePoint startPoint(-0.707, +0.707);
  const ExamplePoint widthsForGaussianProposals(0.1, 0.1);

  Example_SimpleGaussianBankSampler bankSampler(startPoint, bank, widthsForGaussianProposals);
  
  while(true) {
    // Move to the next point
    ++bankSampler;

    // Read the most recent sample:
    const ExamplePoint & mostRecentSample = *bankSampler;
    const ExamplePoint & p = mostRecentSample;

    // Print the most recent sample:
    std::cout << "Most recent sample is [ " 
	      << p.x << "\t" 
	      << p.y << " ] r=\t" << sqrt(p.x*p.x + p.y*p.y) << std::endl;
  }

}


