
#ifndef LESTER_SIMPLEGAUSSIANBANKSAMPLER_H
#define LESTER_SIMPLEGAUSSIANBANKSAMPLER_H

#include "MetropolisSampler.h"
#include "CLHEP/Random/RandGauss.h"

namespace Lester {
  
  /// Restrictions:  PointType must support the array operator ...
  /// i.e. you must be able to do:
  ///        PointType p;
  ///        std::cout << p[0]; // to get the first component
  ///        std::cout << p[1]; // to get the second component etc.kPoints.size()==0);
  /// Also, p must either be copy constructable.  This would allow std::vector, but would
  /// prevent you using "double a[4]", unfortunately.
  template<class PointType, unsigned int numberOfDimensions>
    class SimpleGaussianBankSampler : public MetropolisSampler<PointType> {
  public:
    class BadBank : public std::exception {
    public:
      const char * what() const throw() { 
	return "BadBank : You must use a bank containing at least one bank point!"; 
      }
    };
    class Bank {
      friend class SimpleGaussianBankSampler<PointType, numberOfDimensions>;
    public:
      Bank() {
      }
      void addBankPoint(const PointType & bankPoint) {
	m_bankPoints.push_back(bankPoint);
      }
    private:
      std::vector<PointType> m_bankPoints;
    };
    
    
    /** Constructor .. example of usage:

    // Create a "bank"
    SimpleGaussianBankSampler<PointType>::Bank bank;

    // Fill the "bank"
    bank.addBankPoint(PointType(get));
    bank.addBankPoint(PointType(bank));
    bank.addBankPoint(PointType(points));
    bank.addBankPoint(PointType(from));
    bank.addBankPoint(PointType(somewhere));
    
    // Indicate the point at which the sampler should start:
    PointType startPoint(somewhere);

    // Create the bank sampler ... by giving it the start point
    // and the "bank" which it should use:
    SimpleGaussianBankSampler<PointType>  bankSampler(startPoint, bank);
    
    // Start sampling:
    while(true) {
      PointType nextSampledPoint = bankSampler.sample();
    }

    **/

    SimpleGaussianBankSampler(const PointType & startPoint,
			      const typename SimpleGaussianBankSampler<PointType, numberOfDimensions>::Bank & bank,
			      const PointType & gaussianKernelCharacteristicStepSizes,
			      const double bankStepProbability=0.1) :
      MetropolisSampler<PointType>(startPoint),
      m_bank(bank),
      m_minusLogNBank(-log(bank.m_bankPoints.size())),
      m_widths(gaussianKernelCharacteristicStepSizes),
      m_bankStepProbability(bankStepProbability),
      m_classicStepProbability(1.-bankStepProbability),
      m_logBankStepProbability(log(m_bankStepProbability)),
      m_logClassicStepProbability(log(m_classicStepProbability)) {
      if (bank.m_bankPoints.size()<=0) {
	std::cout << "You must supply at least one bank point!" << std::endl;
	BadBank badBank;
	throw badBank;
      };
    }

  private:
    ////////////////////////////////////////////////////////////////////////////
    // It is our duty to implement the following two virtual functions
    PointType suggestNewPoint() const {
      const PointType & currentPoint = **this;
      return this->proposalBasedOn(currentPoint, m_bank, m_widths);
    }
    double logProbOfSuggestingFirstGivenLast(const PointType& newX, const PointType& oldX) const {

      
      double logClassicPart = m_logClassicStepProbability;
      for (unsigned int i=0; i<numberOfDimensions; ++i) {
	const double tmp = (newX[i]-oldX[i])/m_widths[i];   // Could optimise by storing the reciprocal of the (2*widths squared) and changing the line below.
	logClassicPart -= tmp*tmp*0.5; // we can ignore the root(2 pi sig2) factors, as they will cancel out in the Q factor
      }
      
    
      Lester::LogarithmicTools::IncrementalAdder logBankProb;
      for (unsigned int nb=0; nb<m_bank.m_bankPoints.size(); ++nb) {
	const PointType & bankPoint = m_bank.m_bankPoints[nb];
	double logProbThisBankPoint = m_logBankStepProbability + m_minusLogNBank; // could optimize by storing this fixed sum
	for (unsigned int i=0; i<numberOfDimensions; ++i) {
	const double tmp = (newX[i]-bankPoint[i])/m_widths[i];   // Could optimise by storing the reciprocal of the (2*widths squared) and changing the line below.
	logProbThisBankPoint -= tmp*tmp*0.5; // we can ignore the root(2 pi sig2) factors, as they will cancel out in the Q factor
	}
	logBankProb.addNumberWhoseLogIs(logProbThisBankPoint);
      }
      
      logBankProb.addNumberWhoseLogIs(logClassicPart);
      
      return logBankProb.logOfTotal();
    }
    ////////////////////////////////////////////////////////////////////////////

  private:
    PointType proposalBasedOn(const PointType & x,
			      const Bank & b,
			      const PointType & widths) const {
      
      const bool useClassicStep = (CLHEP::RandFlat::shoot() <= m_classicStepProbability || b.m_bankPoints.size()==0);
      
      // Store the point from which we will send out our tendrils:
      PointType proposalForX(useClassicStep 
			     ? x 
			     : (b.m_bankPoints[CLHEP::RandFlat::shootInt(b.m_bankPoints.size())])
			     ); // making use of copy constructor
      // Send out our tendrils
      for (unsigned int i=0; i<numberOfDimensions; ++i) {
	proposalForX[i] += CLHEP::RandGauss::shoot()*widths[i];
      }
      return proposalForX;
    }
    
    
  private:
    const typename SimpleGaussianBankSampler<PointType, numberOfDimensions>::Bank & m_bank;
    const double m_minusLogNBank;
    const PointType & m_widths;
    const double m_bankStepProbability;
    const double m_classicStepProbability;
    const double m_logBankStepProbability;
    const double m_logClassicStepProbability;
  };

}

#endif
