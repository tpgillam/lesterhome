#ifndef LESTERVIMATHSCONSTANTSH
#define LESTERVIMATHSCONSTANTSH

#include <cmath>

namespace MathsConstants {
  static const double pi=3.1415926535897932384626433832795028841971693993751;
  static const double oneOverPi=1./pi;
  static const double piOverTwo=pi*0.5;
  static const double minusPiOverTwo=-piOverTwo;
  static const double twoPi=2.*pi;
  static const double fourPi=4.*pi;
  static const double sqrtPi=sqrt(pi);
  static const double overThree=1./3.;
  static const double overSqrtThree=1./sqrt(3.);
}

#endif
