
// See test programs in ../Booster (not the old ../booster)

#ifndef TOY_MC_BOOSTER_H
#define TOY_MC_BOOSTER_H

#include "CLHEP/Vector/Rotation.h"
#include "CLHEP/Vector/LorentzVector.h"
#include "CLHEP/Vector/LorentzRotation.h"
#include "CLHEP/Random/RandFlat.h"
#include <cassert>

namespace Booster {

inline CLHEP::Hep3Vector randomDirection() {
  CLHEP::Hep3Vector v(
	       CLHEP::RandFlat::shoot(-1,1),
	       CLHEP::RandFlat::shoot(-1,1),
	       CLHEP::RandFlat::shoot(-1,1)
	       );
  while (v.mag()>1) {
    v.setX(CLHEP::RandFlat::shoot(-1,1));
    v.setY(CLHEP::RandFlat::shoot(-1,1));
    v.setZ(CLHEP::RandFlat::shoot(-1,1));
  };
  v.setMag(1);
  return v;
}

inline CLHEP::Hep3Vector randomTransverseDirection() {
  CLHEP::Hep3Vector v(
	       CLHEP::RandFlat::shoot(-1,1),
	       CLHEP::RandFlat::shoot(-1,1),
	       0
	       );
  while (v.mag()>1) {
    v.setX(CLHEP::RandFlat::shoot(-1,1));
    v.setY(CLHEP::RandFlat::shoot(-1,1));
  };
  v.setMag(1);
  return v;
}

inline void decayInRFAlong(const CLHEP::Hep3Vector directionOfB,
			   const CLHEP::HepLorentzVector a,
			   CLHEP::HepLorentzVector & b,
			   CLHEP::HepLorentzVector & c,
			   const double mb,
			   const double mc) {
  const double ma=a.m();
  assert(ma>0);
  assert(mb>=0);
  assert(mc>=0);
  assert(ma>=(mb+mc));
  // in rest frame, a->b+c, b and c have equal and opposite mementa p:
  const double diffSq=ma*ma-(mb+mc)*(mb+mc);
  const double modP=sqrt(diffSq*(diffSq+4*mb*mc)/(4*ma*ma));
  //cout << modP << endl;
  const CLHEP::Hep3Vector p=modP*(directionOfB.unit());
  //cout << p << endl;
  // so in com frame:
  b.setVectM( p,mb);
  c.setVectM(-p,mc);
  //cout << b << c << endl;
  b.boost(a.boostVector());
  c.boost(a.boostVector());
}

inline void decayIsotropically(const CLHEP::HepLorentzVector a,
			       CLHEP::HepLorentzVector & b,
			       CLHEP::HepLorentzVector & c,
			       const double mb,
			       const double mc) {
  const double ma=a.m();
  assert(ma>0);
  assert(mb>=0);
  assert(mc>=0);
  assert(ma>=(mb+mc));
  // in rest frame, a->b+c, b and c have equal and opposite mementa p:
  const double diffSq=ma*ma-(mb+mc)*(mb+mc);
  const double modP=sqrt(diffSq*(diffSq+4*mb*mc)/(4*ma*ma));
  //cout << modP << endl;
  const CLHEP::Hep3Vector p=modP*randomDirection();
  //cout << p << endl;
  // so in com frame:
  b.setVectM( p,mb);
  c.setVectM(-p,mc);
  //cout << b << c << endl;
  b.boost(a.boostVector());
  c.boost(a.boostVector());
}

inline void decayInTransversePlaneIsotropically(const CLHEP::HepLorentzVector a,
						CLHEP::HepLorentzVector & b,
						CLHEP::HepLorentzVector & c,
						const double mb,
						const double mc) {
  const double ma=a.m();
  assert(ma>0);
  assert(mb>=0);
  assert(mc>=0);
  assert(ma>=(mb+mc));
  // in rest frame, a->b+c, b and c have equal and opposite mementa p:
  const double diffSq=ma*ma-(mb+mc)*(mb+mc);
  const double modP=sqrt(diffSq*(diffSq+4*mb*mc)/(4*ma*ma));
  //cout << modP << endl;
  const CLHEP::Hep3Vector p=modP*randomTransverseDirection();
  //cout << p << endl;
  // so in com frame:
  b.setVectM( p,mb);
  c.setVectM(-p,mc);
  //cout << b << c << endl;
  b.boost(a.boostVector());
  c.boost(a.boostVector());
}


inline void decayIsotropically(const CLHEP::HepLorentzVector a,
			CLHEP::HepLorentzVector & b,
			CLHEP::HepLorentzVector & c,
			CLHEP::HepLorentzVector & d,
			const double m1,
			const double m2,
			const double m3) {
  const double M=a.m();
  assert(M>0);
  assert(m1>=0);
  assert(m2>=0);
  assert(m3>=0);
  assert(M>=(m1+m2+m3));

  bool ok=false;
  do {
    //    cout << "Hi" << endl;
    const double e1= CLHEP::RandFlat::shoot(m1, M-m2-m3);
    const double e2= CLHEP::RandFlat::shoot(m2, M-m1-m3);
    const double e3=M-(e1+e2);
    if (e3>m3) {
      // The energies look good - let's find the angles:
      const double p1      = sqrt(e1*e1-m1*m1);
      const double p2      = sqrt(e2*e2-m2*m2);
      const double p3      = sqrt(e3*e3-m3*m3);
      const double cos1To2 = (p3*p3-(p1*p1+p2*p2))/(2*p1*p2);
      const double cos1To3 = (p2*p2-(p1*p1+p3*p3))/(2*p1*p3);
      //cout << cos1To2 << " " <<cos1To3 << endl;

      if (fabs(cos1To2)<=1. && fabs(cos1To3)<=1.) {
	ok=true;
	//std::cout << "DALITZ " << e1 << " " << e2 << "\n";
	const double ang12   = +acos(cos1To2);
	const double ang13   = -acos(cos1To3);

	const CLHEP::Hep3Vector mom1(p1           , 0            , 0);
	const CLHEP::Hep3Vector mom2(p2*cos(ang12), p2*sin(ang12), 0);
	const CLHEP::Hep3Vector mom3(p3*cos(ang13), p3*sin(ang13), 0);
	//cout << mom1 << endl;
	//cout << mom2 << endl;
	//cout << mom3 << endl;
	//cout << "*****" << endl;

	// So in canonical orientation:
	b.setVectM(mom1, m1); 
	c.setVectM(mom2, m2);
	d.setVectM(mom3, m3);

	// Now randomise the orientation:
	// part one:
	CLHEP::HepRotation matrix;
	matrix.rotate(CLHEP::RandFlat::shoot(0.0, 2*3.14159), mom1);
	// part two:
	const CLHEP::Hep3Vector   bDirec(mom1);
	const CLHEP::Hep3Vector & other = randomDirection();
	const CLHEP::Hep3Vector & axis = bDirec.cross(other);
	const double      angle = fabs(bDirec.angle(other));
	matrix.rotate(angle, axis);
	b.transform(matrix);
	c.transform(matrix);
	d.transform(matrix);

	// Finally, boost to frame of "a":
	b.boost(a.boostVector());
	c.boost(a.boostVector());
	d.boost(a.boostVector());
      };
    };
  } while (!ok);
  
}

}

#endif
