#ifndef LESTER_Simple2DHist_H
#define LESTER_Simple2DHist_H

/* Use like this:


#include "Utils/Simple2DHist.h" 

using namespace Lester::Simple2DHist;

int main() { 
   Hist h(Axis(100,0,1), Axis(100,0,1));
   h.fill(x, y);
}


*/


#include <map>
#include <utility>
#include <cmath>

namespace Lester {

  namespace Simple2DHist {


    struct Bin {
      Bin(double from, double to) : from(from), to(to) {};
      double from;
      double to;
      bool operator<(const Bin & other) const {
	if (from<other.from) return true;
	if (from>other.from) return false;
	return to<other.to;
      }
      double centre() const { return 0.5*(from+to); }
    };

    typedef std::pair<Bin, Bin> Bin2D;

    struct Axis {
      Axis(unsigned int n, double from, double to) :
	n(n), from(from), to(to) {}
      unsigned int n;
      double from;
      double to;
      Bin getBin(double x) const {
	return Bin( from + ((to-from)/n)*floor(n*(x-from)/(to-from)+0),
		    from + ((to-from)/n)*floor(n*(x-from)/(to-from)+1)  );
      }
    };

    struct Hist {
      Hist(Axis xAxis, Axis yAxis) :
	xAxis(xAxis),
	yAxis(yAxis),
	maxWeight(0) {}
      Axis xAxis;
      Axis yAxis;
      
      void fill(double x, double y) {
	const Bin2D & bin = Bin2D( xAxis.getBin(x), yAxis.getBin(y) );
	double w = (data[bin] += 1); // creates bin entry if it doesn't exist
	if (w>maxWeight) {
	  maxWeight=w;
	}
      }
      
      typedef std::map<Bin2D, double> Data;
      Data data;
      double maxWeight;
    };

  }
}

std::ostream & operator<<(std::ostream & os, const Lester::Simple2DHist::Hist & h) {
        using Lester::Simple2DHist::Hist;
        using Lester::Simple2DHist::Bin;
        using Lester::Simple2DHist::Bin2D;
	typedef Hist::Data Data;
	for (Data::const_iterator it=h.data.begin(); it!=h.data.end(); ++it) {
                //os << "Going for another " << std::endl;
        	if (it!=h.data.begin()) {
			os << " ";
		}
		const Bin2D & bin2D = it->first;
		const double weight = it->second;    
  		const Bin & xBin = bin2D.first;
		const Bin & yBin = bin2D.second;
		os << xBin.centre() << " " << yBin.centre() << " " << weight;
	}
	return os;
}

#endif
