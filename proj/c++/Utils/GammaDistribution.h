
#ifndef GAMMADISTRIBUTIONH
#define GAMMADISTRIBUTIONH

// fwd dec
#include "Utils/GammaDistribution.fwd" 

// includes
#include <iostream>
#include "Utils/SampleAndProbabilityModule.h"
#include "CLHEP/Random/RandGauss.h"
#include "Utils/MathsConstants.h"

// declaration
struct GammaDistribution : public SampleAndProbabilityModule<double> {
private:
  const double m_k;
  const double m_theta;
public:
  explicit GammaDistribution(const double k=0, const double theta=1) :
    m_k(k),
    m_theta(theta) {
  }
  double sample() const {
    return CLHEP::RandGauss::shoot(m_mean, m_sigma);
  }
  double propToProbabilityOf(const double & d) const {
    try {
      const double exponent = almostLogOfProbabilityOf(d);
      return exp(exponent);
    } catch (Lester::LogarithmicTools::LogOfZero) {
      return 0;
    };
  }
  double probabilityOf(const double & d) const {
    return m_oneOverSqrtTwoPiSigSq*propToProbabilityOf(d);
  }
  double almostLogOfProbabilityOf(const double & d) const {
    const double delta=d-m_mean;
    const double exponent = delta*delta*m_minusHalfOverSigmaSq;
    return exponent;
  }
  double logOfProbabilityOf(const double & d) const {
    const double ans = 
      almostLogOfProbabilityOf(d) + m_logOfOneOverSqrtTwoPiSigmaSq;
    return ans;
  }
public:
  std::ostream & printMeTo(std::ostream & os) const {
    return os << "GammaDistribution[mean="<<m_mean<<", sigma="<<m_sigma<<"]";
  }
};

inline std::ostream & operator<<(std::ostream & os, const GammaDistribution & obj) {
  return obj.printMeTo(os);
}

#endif

