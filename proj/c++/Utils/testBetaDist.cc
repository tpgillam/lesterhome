
#include "Utils/BetaDistribution.h"
#include <iostream>
#include <cstdlib>

int main(int narg, char*args[]) {
  if (narg!=3) {
    std::cout << "Usage: testBetaDist [alpha] [beta]" << std::endl;
    return 1;
  }

  const BetaDistribution betaDist(atof(args[1]),atof(args[2]));
  while(true) {
    std::cout << betaDist.sample() << std::endl;
  };
};
