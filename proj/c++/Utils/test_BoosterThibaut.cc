
#include "Booster.h"


int main() {
  
  
  const double mCOM = 201; // GeV
  const double mW  = 100; // GeV


  CLHEP::HepLorentzVector w1,w2,e1,nu1;
  const CLHEP::HepLorentzVector com(mCOM,CLHEP::Hep3Vector(0,0,0));

  while(true) {

  Booster::decayIsotropically(com,    w1,w2,    mW,mW);
  Booster::decayIsotropically(w1,    e1,nu1,    0,0);

  std::cout << "THIB " <<  e1.e() << "\n";
  }
  
  return 0;
}
