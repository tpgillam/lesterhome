
#ifndef BETADISTRIBUTIONH
#define BETADISTRIBUTIONH

// fwd dec
#include "Utils/BetaDistribution.fwd" 

// includes
#include <iostream>
#include "Utils/SampleAndProbabilityModule.h"
#include "Utils/MathsConstants.h"
#include "CLHEP/Random/RandFlat.h"
#include <cmath>

// declaration
struct BetaDistribution : public SampleAndProbabilityModule<double> {
private:
  const double m_alpha;
  const double m_beta;
  const double m_oneOverBETA;
  double m_maxWeight;
public:
/*
  BETA distribution:

  x in [0,1]:

  pdf_x(x) =  (x^(alpha-1) (1-x)^(beta-1)) / BETA(alpha,beta)

*/
  explicit BetaDistribution(const double alpha, const double beta) :
    m_alpha(alpha),
    m_beta(beta),
    m_oneOverBETA(1./BETA(alpha,beta)) {

 
    if (alpha<1 || beta<1) {
      throw "This implementation only works for alpha and beta greater than or equal to one";
    }

    const double mode = (alpha-1)/(alpha+beta-2);
    m_maxWeight = propToProbabilityOf(mode);
    // technically that's very bad ... as it won't work for cases in which the pdf is divergent at the mode -- i.e. when alpha or beta are less than one.  So must restrict such cases with the trow on the first line of the constructor:
    //std::cerr << "MAXWEIGHT " << m_maxWeight << " MODE " << mode << std::endl;
    
  }
public:
  // use (possibly inefficient) rejection method:
  double sample() const {

    while(true) {
      const double x = CLHEP::RandFlat::shoot();
      const double y = m_maxWeight*CLHEP::RandFlat::shoot();
      
      if (y<=propToProbabilityOf(x)) {
	return x;
      }
    }
  }
  
private:
  inline double BETA(const double x, const double y) {
    return tgamma(x)*tgamma(y)/tgamma(x+y);
  }
public:
  double propToProbabilityOf(const double & x) const {
    try {
      const double ans = exp(almostLogOfProbabilityOf(x));
      return ans;
    } catch (Lester::LogarithmicTools::LogOfZero l) {
      return 0;
    } catch (Lester::LogarithmicTools::LogOfInfinity i) {
      return HUGE_VAL;
    }
  }
  double probabilityOf(const double & x) const {
    try {
      const double ans = exp(almostLogOfProbabilityOf(x))*m_oneOverBETA;
      return ans;
    } catch (Lester::LogarithmicTools::LogOfZero l) {
      return 0;
    } catch (Lester::LogarithmicTools::LogOfInfinity i) {
      return HUGE_VAL;
    }
  }
  inline double almostLogOfProbabilityOf(const double & x) const {
    if (x>0 && x<1) {
      return (m_alpha-1)*log(x) + (m_beta-1)*log(1.0-x);
    }
    assert (x<=0 || x>=1);
    if (x<0 || x>1) {
      throw  Lester::LogarithmicTools::LogOfZero();
    }
    assert(x==0 || x==1);
    if (x==0) {
      if ((m_alpha-1)>0) {
	throw Lester::LogarithmicTools::LogOfZero();
      } else if ((m_alpha-1)==0) {
	return 0;
      } else {
	assert(m_alpha-1>0);
	throw Lester::LogarithmicTools::LogOfInfinity();
      }
    } else {
      assert(x==1);
      if ((m_beta-1)>0) {
	throw Lester::LogarithmicTools::LogOfZero();
      } else if ((m_beta-1)==0) {
	return 0;
      } else {
	assert(m_beta-1>0);
	throw Lester::LogarithmicTools::LogOfInfinity();
      }
    }
    assert(false);
  }
  double logOfProbabilityOf(const double & x) const {
    return almostLogOfProbabilityOf(x) + log(m_oneOverBETA);
  }
public:
  std::ostream & printMeTo(std::ostream & os) const {
    return os << "BetaDistribution[alpha="<<m_alpha<<", beta="<<m_beta<<"]";
  }
};

inline std::ostream & operator<<(std::ostream & os, const BetaDistribution & obj) {
  return obj.printMeTo(os);
}

#endif

