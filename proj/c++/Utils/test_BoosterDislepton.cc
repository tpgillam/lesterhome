
#include "Booster.h"


int main() {
  
  
  const double mSlepton = 150; // GeV
  const double mNtlino  = 100; // GeV

  const double mP = mNtlino;
  const double mQ = mNtlino;
  const double ma = mSlepton;
  const double mb = mSlepton;
  const double ms = 0;
  const double mt = 0;

  const double perBeamEnergy=7000; // work in units of GeV;
  const CLHEP::HepLorentzVector beam1(perBeamEnergy,CLHEP::Hep3Vector(0,0,+perBeamEnergy));
  const CLHEP::HepLorentzVector beam2(perBeamEnergy,CLHEP::Hep3Vector(0,0,-perBeamEnergy));

  const double x1=(mSlepton*1.1)/perBeamEnergy;
  const double x2=(mSlepton*1.1)/perBeamEnergy;
  const CLHEP::HepLorentzVector z( beam1*x1 + beam2*x2 );

  CLHEP::HepLorentzVector a,b,s,t,P,Q;

  std::cout << "z = " << z << std::endl;

  Booster::decayIsotropically(z,    a,b,    ma,mb);
  Booster::decayIsotropically(a,    s,P,    ms,mP);
  Booster::decayIsotropically(b,    t,Q,    mt,mQ);

  std::cout << "a = " << a << std::endl;
  std::cout << "b = " << b << std::endl;
  std::cout << "P = " << P << std::endl;
  std::cout << "Q = " << Q << std::endl;
  std::cout << "s = " << s << std::endl;
  std::cout << "t = " << t << std::endl;
  
  return 0;
}
