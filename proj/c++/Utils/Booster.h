
// See test programs in ../Booster (not the old ../booster)

#ifndef TOY_MC_BOOSTER_H
#define TOY_MC_BOOSTER_H

#include "CLHEP/Vector/Rotation.h"
#include "CLHEP/Vector/LorentzVector.h"
#include "CLHEP/Vector/LorentzRotation.h"
#include "CLHEP/Random/RandFlat.h"
#include "CLHEP/Units/PhysicalConstants.h"
#include <cassert>
#include <cmath>
#include <vector>
#include <cstdlib> // for qsort

namespace Booster {

inline CLHEP::Hep3Vector randomDirection() {
  CLHEP::Hep3Vector v(
	       CLHEP::RandFlat::shoot(-1,1),
	       CLHEP::RandFlat::shoot(-1,1),
	       CLHEP::RandFlat::shoot(-1,1)
	       );
  while (v.mag()>1) {
    v.setX(CLHEP::RandFlat::shoot(-1,1));
    v.setY(CLHEP::RandFlat::shoot(-1,1));
    v.setZ(CLHEP::RandFlat::shoot(-1,1));
  };
  v.setMag(1);
  return v;
}

inline CLHEP::Hep3Vector randomTransverseDirection() {
  CLHEP::Hep3Vector v(
	       CLHEP::RandFlat::shoot(-1,1),
	       CLHEP::RandFlat::shoot(-1,1),
	       0
	       );
  while (v.mag()>1) {
    v.setX(CLHEP::RandFlat::shoot(-1,1));
    v.setY(CLHEP::RandFlat::shoot(-1,1));
  };
  v.setMag(1);
  return v;
}

inline void decayInRFAlong(const CLHEP::Hep3Vector directionOfB,
			   const CLHEP::HepLorentzVector a,
			   CLHEP::HepLorentzVector & b,
			   CLHEP::HepLorentzVector & c,
			   const double mb,
			   const double mc) {
  const double ma=a.m();
  assert(ma>0);
  assert(mb>=0);
  assert(mc>=0);
  assert(ma>=(mb+mc));
  // in rest frame, a->b+c, b and c have equal and opposite mementa p:
  const double diffSq=ma*ma-(mb+mc)*(mb+mc);
  const double modP=sqrt(diffSq*(diffSq+4*mb*mc)/(4*ma*ma));
  //cout << modP << endl;
  const CLHEP::Hep3Vector p=modP*(directionOfB.unit());
  //cout << p << endl;
  // so in com frame:
  b.setVectM( p,mb);
  c.setVectM(-p,mc);
  //cout << b << c << endl;
  b.boost(a.boostVector());
  c.boost(a.boostVector());
}

inline void decayIsotropically(const CLHEP::HepLorentzVector a,
			       CLHEP::HepLorentzVector & b,
			       CLHEP::HepLorentzVector & c,
			       const double mb,
			       const double mc) {
  const double ma=a.m();
  assert(ma>0);
  assert(mb>=0);
  assert(mc>=0);
  assert(ma>=(mb+mc));
  // in rest frame, a->b+c, b and c have equal and opposite mementa p:
  const double diffSq=ma*ma-(mb+mc)*(mb+mc);
  const double modP=sqrt(diffSq*(diffSq+4*mb*mc)/(4*ma*ma));
  //cout << modP << endl;
  const CLHEP::Hep3Vector p=modP*randomDirection();
  //cout << p << endl;
  // so in com frame:
  b.setVectM( p,mb);
  c.setVectM(-p,mc);
  //cout << b << c << endl;
  b.boost(a.boostVector());
  c.boost(a.boostVector());
}

inline void decayInTransversePlaneIsotropically(const CLHEP::HepLorentzVector a,
						CLHEP::HepLorentzVector & b,
						CLHEP::HepLorentzVector & c,
						const double mb,
						const double mc) {
  const double ma=a.m();
  assert(ma>0);
  assert(mb>=0);
  assert(mc>=0);
  assert(ma>=(mb+mc));
  // in rest frame, a->b+c, b and c have equal and opposite mementa p:
  const double diffSq=ma*ma-(mb+mc)*(mb+mc);
  const double modP=sqrt(diffSq*(diffSq+4*mb*mc)/(4*ma*ma));
  //cout << modP << endl;
  const CLHEP::Hep3Vector p=modP*randomTransverseDirection();
  //cout << p << endl;
  // so in com frame:
  b.setVectM( p,mb);
  c.setVectM(-p,mc);
  //cout << b << c << endl;
  b.boost(a.boostVector());
  c.boost(a.boostVector());
}

  
  inline double PDK__PRIVATE(const double a, const double b, const double c) {
    //the PDK function
    return sqrt((a-b-c)*(a+b+c)*(a-b+c)*(a+b-c))/(2.*a);
  }
  
  inline double calcMaxWeight__PRIVATE(const double fNt, const double fTeCmTm,const std::vector<double> & fMass) {
    double emmax = fTeCmTm + fMass[0];
    double emmin = 0;
    double wtmax = 1;
    for (int n=1; n<fNt; n++) {
      emmin += fMass[n-1];
      emmax += fMass[n];
      wtmax *= PDK__PRIVATE(emmax, emmin, fMass[n]);
    }
    return wtmax;
    
  }


  int DoubleMax__PRIVATE (const void *a, const void *b) 
  {
    //special max function
    double aa = * ((double *) a); 
    double bb = * ((double *) b); 
    if (aa > bb) return  1;
    if (aa < bb) return -1;
    return 0;
    
  }

  // Generate decays according to N-body phase space, for N=2 to 18.
  // By default, generated unweighted events, but CAN generate weighted events too if desired.  If weighted events are used, weight is between 0 and 1.
  // If decay is impossible, return zero sized vector in decayProducts.
  //
  inline std::vector<CLHEP::HepLorentzVector>
  decayIsotropically(const CLHEP::HepLorentzVector & P, // INPUT
		     const std::vector<double> & mass, // INPUT
		     double & weight, // max Weight is one // OUTPUT
		     const bool unweight = true) {

    
    // Based on TGenPhaseSpace.cxx implementation of Raubold-Lynch method
    // The code is based on the GENBOD function (W515 from CERNLIB)
    //  using the Raubold and Lynch method
    //      F. James, Monte Carlo Phase Space, CERN 68-15 (1968)

    const unsigned int kMAXP = 18;

    const std::vector<double> & fMass = mass;
    
    const std::vector<CLHEP::HepLorentzVector> nullAns;
    
    const int fNt = mass.size(); // number of decay products
    
    if (fNt<2 || fNt>kMAXP) return nullAns;  // no more then 18 particles
    
    double fTeCmTm__ = P.m(); // total energy in C.M. minus the sum of the masses
    for (int n=0; n<fNt; ++n) {
      fTeCmTm__   -= mass[n];
    }
    
    if (fTeCmTm__ <=0) return nullAns; // not enough energy for this decay
    
    const double fTeCmTm = fTeCmTm__; // total energy in C.M. minus the sum of the masses
    
    const double fWtMax = 1./(calcMaxWeight__PRIVATE(fNt, fTeCmTm, fMass));

    const CLHEP::Hep3Vector boostVector = P.boostVector();  
    /*    

      if (P.Beta()) {
      Double_t w = P.Beta()/P.Rho();
      fBeta[0] = P(0)*w;
      fBeta[1] = P(1)*w;
      fBeta[2] = P(2)*w;
      } else {
      fBeta[0]=fBeta[1]=fBeta[2]=0;
      }
    */
    
    //////////////////// END OF SET UP
    
    while(true) {
      // LOOP IS FOR UNWEIGHTING EVENTS!
    

    std::vector<CLHEP::HepLorentzVector> fDecPro(fNt); // the "answer"

    //  Generate a random final state.
    //  The function returns the weigth of the current event.
    //  The TLorentzVector of each decay product can be obtained using GetDecay(n).
    //
    // Note that Momentum, Energy units are Gev/C, GeV

    
    double rno[kMAXP];
    rno[0] = 0;
    if (fNt>2) {
      for (int n=1; n<fNt-1; ++n)  rno[n]=CLHEP::RandFlat::shoot();   // fNt-2 random numbers
      qsort(rno+1 ,fNt-2 ,sizeof(double) ,DoubleMax__PRIVATE);  // sort them
    }
    rno[fNt-1] = 1;
    
    double invMas[kMAXP], sum=0;
    for (int n=0; n<fNt; ++n) {
      sum      += fMass[n];
      invMas[n] = rno[n]*fTeCmTm + sum;
    }
    
    //
    //-----> compute the weight of the current event
    //
    double wt=fWtMax;
    double pd[kMAXP];
    for (int n=0; n<fNt-1; ++n) {
      pd[n] = PDK__PRIVATE(invMas[n+1],invMas[n],fMass[n+1]);
      wt *= pd[n];
    }
    
    // Note that in the 6 lines above wt was initialised to fWtMax (the reciprocal of the maxWeight) and thus the value at present in wt is somewhere between 0 and 1.
    
    // UNWEIGHT EVENTS:
    if (unweight && wt < CLHEP::RandFlat::shoot()) continue; // LESTER ADDITION
    
    weight = (unweight?1:wt);
      
   
    //
    //-----> complete specification of event (Raubold-Lynch method)
    //
    fDecPro[0].set/*xyze*/(0, pd[0], 0 , sqrt(pd[0]*pd[0]+fMass[0]*fMass[0]) );
    
    int i=1;
    while (1) {
      fDecPro[i].set/*xyze*/(0, -pd[i-1], 0 , sqrt(pd[i-1]*pd[i-1]+fMass[i]*fMass[i]) );
      
      double cZ   = 2.*CLHEP::RandFlat::shoot() - 1;
      double sZ   = sqrt(1-cZ*cZ);
      double angY = 2.*CLHEP::pi * CLHEP::RandFlat::shoot();
      double cY   = cos(angY);
      double sY   = sin(angY);
      for (int j=0; j<=i; ++j) {
	CLHEP::HepLorentzVector & v = fDecPro[j];
	double x = v.x();
	double y = v.y();
	v.setPx( cZ*x - sZ*y );
	v.setPy( sZ*x + cZ*y );   // rotation around Z
	x = v.x();
	double z = v.z();
	v.setPx( cY*x - sY*z );
	v.setPz( sY*x + cY*z );   // rotation around Y
      }
      
      if (i == (fNt-1)) break;
      
      double beta = pd[i] / sqrt(pd[i]*pd[i] + invMas[i]*invMas[i]);
      for (int j=0; j<=i; ++j){
	fDecPro[j].boostY(beta); 
      }
      ++i;
    } // end while
    
      //
      //---> final boost of all particles  
      //
    for (int n=0; n<fNt; ++n) fDecPro[n].boost(boostVector);
    
    //
    //---> return the weigth of event
    //
    
    
    
    return fDecPro;
    
    
     
    } // END OF LOOP FOR UNWEIGHTING EVENTS
    
  }
  
  
  
  inline void decayIsotropically(const CLHEP::HepLorentzVector a,
			CLHEP::HepLorentzVector & b,
			CLHEP::HepLorentzVector & c,
			CLHEP::HepLorentzVector & d,
			const double m1,
			const double m2,
			const double m3) {
  const double M=a.m();
  assert(M>0);
  assert(m1>=0);
  assert(m2>=0);
  assert(m3>=0);
  assert(M>=(m1+m2+m3));

  bool ok=false;
  do {
    //    cout << "Hi" << endl;
    const double e1= CLHEP::RandFlat::shoot(m1, M-m2-m3);
    const double e2= CLHEP::RandFlat::shoot(m2, M-m1-m3);
    const double e3=M-(e1+e2);
    if (e3>m3) {
      // The energies look good - let's find the angles:
      const double p1      = sqrt(e1*e1-m1*m1);
      const double p2      = sqrt(e2*e2-m2*m2);
      const double p3      = sqrt(e3*e3-m3*m3);
      const double cos1To2 = (p3*p3-(p1*p1+p2*p2))/(2*p1*p2);
      const double cos1To3 = (p2*p2-(p1*p1+p3*p3))/(2*p1*p3);
      //cout << cos1To2 << " " <<cos1To3 << endl;

      if (fabs(cos1To2)<=1. && fabs(cos1To3)<=1.) {
	ok=true;
	//std::cout << "DALITZ " << e1 << " " << e2 << "\n";
	const double ang12   = +acos(cos1To2);
	const double ang13   = -acos(cos1To3);

	const CLHEP::Hep3Vector mom1(p1           , 0            , 0);
	const CLHEP::Hep3Vector mom2(p2*cos(ang12), p2*sin(ang12), 0);
	const CLHEP::Hep3Vector mom3(p3*cos(ang13), p3*sin(ang13), 0);
	//cout << mom1 << endl;
	//cout << mom2 << endl;
	//cout << mom3 << endl;
	//cout << "*****" << endl;

	// So in canonical orientation:
	b.setVectM(mom1, m1); 
	c.setVectM(mom2, m2);
	d.setVectM(mom3, m3);

	// Now randomise the orientation:
	// part one:
	CLHEP::HepRotation matrix;
	matrix.rotate(CLHEP::RandFlat::shoot(0.0, 2*3.14159), mom1);
	// part two:
	const CLHEP::Hep3Vector   bDirec(mom1);
	const CLHEP::Hep3Vector & other = randomDirection();
	const CLHEP::Hep3Vector & axis = bDirec.cross(other);
	const double      angle = fabs(bDirec.angle(other));
	matrix.rotate(angle, axis);
	b.transform(matrix);
	c.transform(matrix);
	d.transform(matrix);

	// Finally, boost to frame of "a":
	b.boost(a.boostVector());
	c.boost(a.boostVector());
	d.boost(a.boostVector());
      };
    };
  } while (!ok);
  
}

}

#endif
