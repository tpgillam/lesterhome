#ifndef LESTER_DIALOG_H
#define LESTER_DIALOG_H

#include <string>
#include <cstdio>

std::string getAllResponseTo(const std::string & question) {

  std::string s = std::string("zenity --entry --text \"") +question + std::string("\"");
  FILE * zenity = popen(s.c_str(), "r");
  char buff[512];

  fgets(buff, sizeof(buff), zenity); {
    std::cout << "You answered ["<<buff<<"]" << std::endl;
  }
  std::string ans = buff;
  // remove last newline:
  if (!(ans.empty())) {
    if (ans.at(ans.length() - 1 )=='\n') {
      ans.erase(ans.length() - 1);
    }
  }
  return ans;
}


#endif

