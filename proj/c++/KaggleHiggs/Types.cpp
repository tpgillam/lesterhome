#include "Types.h"

#include "Utils/DereferencingOstreamIterator.h"
#include "KaggleEventInvisWithWeights.h"

std::ostream & operator<<(std::ostream & os, const InvisSolns & solns) {
 copy(solns.begin(), solns.end(), DereferencingOstreamIterator<boost::shared_ptr<KaggleEventInvisWithWeights >, 1>(std::cout , ",\n"));
 return os;
}
