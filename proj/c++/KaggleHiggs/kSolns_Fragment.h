const double kSoln1 = 
(tz*(-chiSq + mTauSq - 2.*deltax*tx + 2.*pXMissOnTwo*tx - 2.*deltay*ty + 
       2.*pYMissOnTwo*ty - leicesterSquare(tm)) - te*sqrt(kDiscriminant))/
  (2.*(te - tz)*(te + tz))
;
const double kSoln2 = 
(tz*(-chiSq + mTauSq - 2.*deltax*tx + 2.*pXMissOnTwo*tx - 2.*deltay*ty + 
       2.*pYMissOnTwo*ty - leicesterSquare(tm)) + te*sqrt(kDiscriminant))/
  (2.*(te - tz)*(te + tz))
;
