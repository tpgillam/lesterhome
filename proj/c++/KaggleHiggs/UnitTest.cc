#include "KaggleEventVis.h"
#include "CanonicalKaggleEventVis.h"
#include "KaggleEventFull.h"
#include "MarkovChainSampler/MetropolisSampler.h"
#include "CLHEP/Vector/ThreeVector.h"
#include "Types.h"
#include "resources.h"
#include <fstream>

inline bool near (double x, double y, double tol=0.000001) {
  return fabs(x-y)<fabs(tol);
}

int main(int argc, char * argv[]) {


  try {
    {
      KaggleEventVis a;
      a.sMu.set(1,2,3,CLHEP::Tcomponent(4));
      a.tMu.set(CLHEP::Tcomponent(4),1,2,3);
      std::cout << a << std::endl;
      if (a.tMu != a.sMu) {
        return 1;
      }
    }

    {
      KaggleEventInvis a;
      a.pMu.set(CLHEP::Tcomponent(4),1,2,3);
      a.qMu.set(CLHEP::Tcomponent(4),1,2,3);
      std::cout << a << std::endl;
    }

    {
      KaggleEventFull a;
      a.vis.sMu.set(1,2,3,CLHEP::Tcomponent(4));
      a.vis.tMu.set(CLHEP::Tcomponent(4),1,2,3);
      a.invis.pMu.set(CLHEP::Tcomponent(4),1,2,3);
      a.invis.qMu.set(CLHEP::Tcomponent(4),1,2,3);
      std::cout << a << std::endl;
    }
    /*
        {
          KaggleEventVis a;
          a.sMu.set(6,8,7,CLHEP::Tcomponent(42));
          a.tMu.set(9,12,7,CLHEP::Tcomponent(43));
          a.pTMiss.set(3,4);
          const CanonicalKaggleEventVis c(a.getCanonicalForm());
          std::cout << c << " should be canonical form of " << a << std::endl;
          if (!near(c.pTMiss.x(),5)) {
            return 2;
          }
          if (c.pTMiss.y() != 0) {
            return 2;
          }
          if (!near(c.sMu.x(),10)) {
            return 3;
          }
          if (!near(c.sMu.y(),0)) {
            return 4;
          }
          if (!near(c.tMu.x(),15)) {
            return 5;
          }
          if (!near(c.tMu.y(),0)) {
            return 6;
          }
        }
    */

    {
      KaggleEventVis a;
      a.sMu.setVectM(CLHEP::Hep3Vector(1,2,3),tauMass/2);
      a.tMu.setVectM(CLHEP::Hep3Vector(4,-12,7),tauMass/3);
      a.pTMiss.set(.03,.04);
//     const CanonicalKaggleEventVis c(a.getCanonicalForm());
      //    std::cout << c << " should be canonical form of " << a << std::endl;
      std::cout << "Trying something hard" << std::endl;
      const double deltax=0;
      const double deltay=0;
      const double chiSq=0;
      InvisSolns solns;
      findPhaseSpaceSolns(a, deltax, deltay, chiSq, // INPUTS
                          solns); // OUTPUT
      if (!(solns.size()==4)) {
        return 7;
      }
      std::cout << "Finished something hard" << std::endl;
      std::cout << "Here are the solns: {\n" << solns << "}" << std::endl;

      std::cout << "Checking they satisfy the tau mass and ptmiss constraints:" << std::endl;

      int i = 0;
      for (InvisSolns::const_iterator it = solns.begin();
           it != solns.end();
           ++it) {
        ++i;
        std::cout << "checking soln " << i << " hadronic tau constraint " << std::endl;
        if (!near(  ((*it)->pMu + a.sMu).m(), tauMass )) {
          return 8;
        }
        std::cout << "checking soln " << i << " leptonic tau constraint " << std::endl;
        if (!near(  ((*it)->qMu + a.tMu).m(), tauMass )) {
          return 9;
        }
        std::cout << "checking soln " << i << " ptmiss constraint " << std::endl;

        if (!near((  CLHEP::Hep2Vector((*it)->qMu + (*it)->pMu) -  CLHEP::Hep2Vector(a.pTMiss)   ).mag(), 0 )) {
          std::cout << CLHEP::Hep2Vector((*it)->qMu + (*it)->pMu) << std::endl;
          std::cout << CLHEP::Hep2Vector(a.pTMiss) << std::endl;
          return 10;
        }
      }

    }

    {
      // Read in some events:
      std::string kind;
      KaggleEventVis vis;
      KaggleEventInvis invis;


      int i=0;
      int s=0;
      int b=0;
      std::ifstream f("eventsForChrisFormatted.txt");
      while (getNextLine(f, kind, vis, invis)) {

        if (kind=="#") {
          std::cout << "parser not absorbing comments" << std::endl;
          return 11;
        }

        ++i;
        std::cout << "Read event " << i << " " << kind << " " << vis << " " << invis << std::endl;
        if (kind=="b") ++b;
        if (kind=="s") ++s;
      }
      f.close();

      if (i!=20) {
        std::cout << "Expected 20 events" << std::endl;
        return 12;
      }
      if (s!=10) {
        std::cout << "Expected 10 s events" << std::endl;
        return 13;
      }
      if (b!=10) {
        std::cout << "Expected 10 b events" << std::endl;
        return 14;
      }

    }

    {
      // Read in a signal event and use it:
      std::cout << "\nInitial Soln Test\n\n" << std::flush;
      std::string kind;
      KaggleEventVis vis;
      KaggleEventInvis invis;


      std::ifstream f("eventsForChrisFormatted.txt");
      //// Try to find a signal event:
      //while (getNextLine(f, kind, vis, invis) && kind != "s") ; // yes, do nothing
      //if (kind!="s") {
      //  std::cout << "Bad read logic" << std::endl;
      //  return 15;
      // }
      //
      while (getNextLine(f, kind, vis, invis)) {

        //std::cout << "Read signal event " << kind << " " << vis << " " << invis << "\n" << std::endl;

        Hypothesis hyp;
        unsigned int attempts;

        findValidStart(
          // INPUTS:
          vis,
          // OUTPUTS:
          hyp, attempts); // This call will throw a std::string if it contains broken logic

        std::cout << "Succcess at finding initial soln in " << attempts << " attempts.\t"
                  << hyp
                  << " for " << kind << " " << vis
                  << std::endl;

        Parameters params;
        params.resonanceMass = invis.mResonance; // Here in the UnitTest we will be friendly and set it to the "truth" value.
        params.resonanceMassConstraintWidth = 0.1; // ... in units used in the input file (GeV)
        params.cauchy = false;

        ExperimentalParameters eParams;
        eParams.pTMissConstraintWidth = 0.1; // ... in units used in the input file (GeV)

        const double logLik = propToLogLikelihood(vis, /*given*/ hyp, params, eParams);  // This will throw if the startPoint is actually not good -- which shouldn't be the case!
        std::cout << "logLik = " << logLik << std::endl;
      }

      f.close();
    }

    {
      // Read in a signal event and use it:
      std::cout << "\nInitial Soln Test\n\n" << std::flush;
      std::string kind;
      KaggleEventVis vis;
      KaggleEventInvis invis;

      // Find a particular event:
      unsigned int eventNumberToWorkOn = 4;
      {
        std::ifstream f("eventsForChrisFormatted.txt");
        for (int i=0; i<eventNumberToWorkOn; ++i) {
          getNextLine(f, kind, vis, invis);
        }
        f.close();
      }

      Hypothesis initialHypothesis;
      unsigned int attempts;

      findValidStart(
        // INPUTS:
        vis,
        // OUTPUTS:
        initialHypothesis, attempts); // This call will throw a std::string if it contains broken logic

      std::cout << "Succcess at finding initial soln in " << attempts << " attempts.\t"
                << initialHypothesis
                << " for " << kind << " " << vis
                << std::endl;


      Parameters params;
      params.resonanceMass = invis.mResonance; // Here in the UnitTest we will be friendly and set it to the "truth" value.
      params.resonanceMassConstraintWidth = 1; // ... in units used in the input file (GeV)
      params.cauchy = false;

      ExperimentalParameters eParams;
      eParams.pTMissConstraintWidth = 5; // ... in units used in the input file (GeV)

      MySimpleSampler sampler(initialHypothesis, vis, params, eParams);

      for (int i=0; i<10000; ++i) {
        const Hypothesis & currentHyp = *sampler;
        if (i%100==0) {
          std::cout
              << "oldevt " << eventNumberToWorkOn
              << ", current point: " << sampler.logProbabilityOfLastAcceptedPoint()
              << " " << currentHyp
              << " " << invis.qMu.m2()
              << " " << vis.pTMiss
              << std::endl;
        }
        ++sampler;
      }
      const double currentLogLik = sampler.logTargetDistribution(*sampler);
      if (currentLogLik < -40) {
        std::cout << "Failed to do a good search in " << __FILE__ << " : " << __LINE__ << " ...." << std::endl;
        return 20;
      }

    }

    if (true) {
      // Read in a signal event and use it:
      std::cout << "\nTest Combined S+B sampler\n\n" << std::flush;
      std::string kind;
      KaggleEventVis vis;
      KaggleEventInvis invis;

      // Find a particular event:
      unsigned int eventNumberToWorkOn = argc-1; // 1 to 10 are bg, 11 to 20 are sig
      {
        std::ifstream f("eventsForChris_nomMasses.txt");
        for (int i=0; i<eventNumberToWorkOn; ++i) {
          getNextLine(f, kind, vis, invis);
        }
        f.close();
      }

      Hypothesis initialHypothesis;
      unsigned int attempts;

      findValidStart(
        // INPUTS:
        vis,
        // OUTPUTS:
        initialHypothesis, attempts); // This call will throw a std::string if it contains broken logic

      std::cout << "Succcess at finding initial soln in " << attempts << " attempts.\t"
                << initialHypothesis
                << " for " << kind << " " << vis
                << std::endl;

      Hypothesis betterInitialHypothesis;
      {
        // TRAIN TO SIGNAL NODE
        Parameters params;
        params.resonanceMass = 125; // Here in the UnitTest we will be friendly and set it to the "truth" value.
        params.resonanceMassConstraintWidth = 0.1; // ... in units used in the input file (GeV)
        params.cauchy = false;

        ExperimentalParameters eParams;
        eParams.pTMissConstraintWidth = 5; // ... in units used in the input file (GeV)

        MySimpleSampler sampler(initialHypothesis, vis, params, eParams);
        Hypothesis currentHyp;

        for (int i=0; i<10000; ++i) {
          currentHyp = *sampler;
          if (i%100==0) {
            std::cout
                << "oldevt " << eventNumberToWorkOn
                << ", current point: " << sampler.logProbabilityOfLastAcceptedPoint()
                << " " << currentHyp
                << " " << invis.qMu.m2()
                << " " << vis.pTMiss
                << std::endl;
          }
          ++sampler;
        }
        const double currentLogLik = sampler.logTargetDistribution(*sampler);
        if (currentLogLik < -40) {
          std::cout << "Failed to do a good search in " << __FILE__ << " : " << __LINE__ << " ...." << std::endl;
          return 30;
        }

        betterInitialHypothesis = currentHyp;
      }



      Parameters sParams;
      sParams.resonanceMass = 125; // Here in the UnitTest we will be friendly and set it to the "truth" value.
      sParams.resonanceMassConstraintWidth = 0.1; // ... in units used in the input file (GeV)
      sParams.cauchy = true;

      Parameters bParams;
      bParams.resonanceMass = 91.2; // Here in the UnitTest we will be friendly and set it to the "truth" value.
      bParams.resonanceMassConstraintWidth = 2.4; // ... in units used in the input file (GeV)
      bParams.cauchy = true;

      ExperimentalParameters eParams;
      eParams.pTMissConstraintWidth = 5; // ... in units used in the input file (GeV)

      debugResoOn = true;
      MyNextSampler sampler(betterInitialHypothesis, vis, sParams, bParams, eParams);

      for (int i=0; true; ++i) {
        const Hypothesis & currentHyp = *sampler;
        std::cout
            << "evt " << eventNumberToWorkOn
            << ", current point: " << sampler.logProbabilityOfLastAcceptedPoint()
            << " " << currentHyp
            << " " << invis.qMu.m2()
            << " " << vis.pTMiss
            << std::endl;
        ++sampler;
      }
    }

    std::cout << std::endl;
    return 0;
  } catch (...) {
    std::cout << "Some exception was thrown." << std::endl;
    return -1;
  }
}













