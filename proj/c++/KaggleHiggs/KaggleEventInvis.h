#ifndef LESTERKAGGLEEVENTINVISH
#define LESTERKAGGLEEVENTINVISH

class KaggleEventInvis; // fwd dec

#include <ostream>
#include "CLHEP/Vector/LorentzVector.h"

class KaggleEventInvis {
 public:
  CLHEP::HepLorentzVector pMu; // neutrino from hadronic tau
  CLHEP::HepLorentzVector qMu; // sum of first and second neutrino from leptonic tau
  double mResonance; // should not really be here ... move out at a later date ...
};

inline std::ostream & operator<<(std::ostream & os, const KaggleEventInvis & event) {
  return os << "KaggleEventInvis[neuHad(pMu)="<<event.pMu<<", neusLep(qMu)="<<event.qMu<<", mResonance="<<event.mResonance<<"]";
}

inline std::istream & operator>>(std::istream & is, KaggleEventInvis & k) {
  double e,px,py,pz;
  is >> e >> px >> py >> pz;
  k.pMu = CLHEP::HepLorentzVector(CLHEP::Tcomponent(e), px, py, pz);
  is >> e >> px >> py >> pz;
  k.qMu = CLHEP::HepLorentzVector(CLHEP::Tcomponent(e), px, py, pz);
  is >> e;
  k.mResonance = e;
  return is;
}

#endif

