#!/bin/sh

make UnitTest || exit

killall UnitTest
killall main

export num=6000000
export DIR=PLOTS/1

mkdir -p $DIR

rm -rf $DIR/fifo* ; mkfifo $DIR/fifo{1,2,3,4,5,6,7,8,9,10} $DIR/fifo{11,12,13,14,15,16,17,18,19,20}

( for i in `seq 1 10` ; do { ./UnitTest `seq 1 $i` | grep ^RESO  | gawk '{print "'$i'",$2}' | head -$num > $DIR/fifo$i & } done ; ) ; paste -d '         \n' $DIR/fifo{1,2,3,4,5,6,7,8,9,10} | hist multi l 50 u 150 n 200 title backgroundEvents mark quit png backgr.png pdf backgr.pdf > /dev/null &


( for i in `seq 11 20` ; do { ./UnitTest `seq 1 $i` | grep ^RESO  | gawk '{print "'$i'",$2}'| head -$num > $DIR/fifo$i & } done ; ) ; paste -d '         \n' $DIR/fifo{11,12,13,14,15,16,17,18,19,20} | hist multi l 50 u 150 n 200 title signalEvents quit png signal.png pdf signal.pdf > /dev/null &


wait

