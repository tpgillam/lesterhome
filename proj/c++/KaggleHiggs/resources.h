#ifndef LESTERRESOURCESH
#define LESTERRESOURCESH


#include "PDG.h"
#include "Simple.h"
#include <cstdlib> // for abort
#include "Types.h"
#include "KaggleEventInvisWithWeights.h"
#include "Hypothesis.h"
#include "Parameters.h"
#include "ExperimentalParameters.h"
#include "CLHEP/Random/RandGauss.h"

// returns true on success
bool getNextLine(std::istream & f,
                 // OUTPUTS:
                 std::string & kind,
                 KaggleEventVis & vis,
                 KaggleEventInvis & invis) {
  while (f >> kind) {

    if (kind=="#") {
      // this is a comment line, so absorb rest of line:
      std::string buf;
      std::getline(f, buf);
      continue; // try next line
    }

    // if we are here, we are reading in.
    f >> vis >> invis;
    return true;
  }
  return false;
}



// This is based on pieces of output from the mathematica notebook in this directory.

// Calculate a (nonlinearly transformed by order preserving transformation of) the ratio of the likelihood (according to phase space alone) of our visible kaggle event under the signal hypothesis divided by that same likelihood under the background hypothesis.  See mathematica notebook in this directory, and ~30% of way in LabBook 12D.

void findPhaseSpaceSolns(
  // INPUTS:
  const KaggleEventVis & ev, const double deltax, const double deltay, const double chiSq,
  // OUTPUTS:
  InvisSolns & invisSolns // these are those consistent with the input deltax, deltay and chiSq.
) {
  invisSolns.clear();
  invisSolns.reserve(4);

  const double pXMiss=ev.pTMiss.x();
  const double pYMiss=ev.pTMiss.y();

  const double pXMissOnTwo = pXMiss/2.0;
  const double pYMissOnTwo = pYMiss/2.0;

  // s represents four-mom of visible part of hadronic tau:
  const CLHEP::HepLorentzVector & sMu = ev.sMu;
  const double se = sMu.e();
  const double sx = sMu.x();
  const double sy = sMu.y();
  const double sz = sMu.z();
  const double sm = sMu.m();

  // t represents four-mom of lepton from other tau:
  const CLHEP::HepLorentzVector & tMu = ev.tMu;
  const double te = tMu.e();
  const double tx = tMu.x();
  const double ty = tMu.y();
  const double tz = tMu.z();
  const double tm = tMu.m();

#include "hDiscriminant_Fragment.h"

  if (hDiscriminant<0) {
    return;
    //throw Lester::LogarithmicTools::LogOfZero();
  }

#include "kDiscriminant_Fragment.h"

  if (kDiscriminant<0) {
    return;
    //throw Lester::LogarithmicTools::LogOfZero();
  }

#include "hSolns_Fragment.h"
#include "kSolns_Fragment.h"

  KaggleEventInvis tmp;
  CLHEP::HepLorentzVector & pMu = tmp.pMu;
  CLHEP::HepLorentzVector & qMu = tmp.qMu;

  for (int i=0; i<2; ++i) {
    const double h = ((i==0) ? hSoln1 : hSoln2);
    pMu.setVectM(CLHEP::Hep3Vector(pXMissOnTwo+deltax, pYMissOnTwo+deltay, h), 0);
    // pMu is the four mom of the invisible neutrino associated with the hadronic tau.
    // std::cout << "TOP WORKS : " << (pMu + sMu).m() << " " << tauMass << std::endl;


    for (int j=0; j<2; ++j) {
      const double k = ((j==0) ? kSoln1 : kSoln2);
      qMu.setVectM(CLHEP::Hep3Vector(pXMissOnTwo-deltax, pYMissOnTwo-deltay, k), sqrt(chiSq));
      // qMu is the four mom of the SUM of the two neutrinos associated with the leptonically decauing tau.
      // It has a massSq of chiSq since the opening angle between the neutrinos may give it a non-zero mass.

      // std::cout << "            BOT WORKS : " << (qMu + tMu).m() << " " << tauMass << std::endl;



      const  double dTauMassSq1ByDH = // accounts for hadronic tau mass shell contstraint enforcement
#include "dTauMassSq1ByDH_Fragment.h"
        ;
      const  double dTauMassSq2ByDK = // accounts for leptonc tau mass shell constraint enforcement.
#include "dTauMassSq2ByDK_Fragment.h"
        ;

      const double one=1; // accounts for ptmiss constraint enforecement

      const double weightFromConstraints = 1./fabs(dTauMassSq1ByDH * dTauMassSq2ByDK * one);
      const double weightFromFinalState = 1./( pMu.e() * qMu.e() * sMu.e() * tMu.e()       ); // we ignore all the factors of 2 that should be in here, since they just amount to an overall constant which we can ignore.
      Weights w(weightFromConstraints, weightFromFinalState);

      boost::shared_ptr<KaggleEventInvisWithWeights> soln(new KaggleEventInvisWithWeights(tmp, w));

      invisSolns.push_back(soln);

      //  std::cout << hDiscriminant << " " << kDiscriminant << " " << dTauMassSq1ByDH << " " << dTauMassSq2ByDK << " " << pMu << " " << qMu << std::endl;

      //std::cout << (qMu + tMu).m() << " " << tauMass << std::endl;
    }
  }
}


// pXMiss and pYMiss are in reality never measured perfectly.  We expect some experimental resolution.
// We take advantage of that here.  It is necessary to pull the 'measured' pXMiss and pYMiss out of alignment to the positions indicated below, in order to be sure of getting all disciminants non-negative at the start. Later we hope that the likelihood (which will heavily supress badly mismeasured pTMiss) will pull the other parameters into a more sensible region ..
void findValidStart(
  // INPUTS:
  const KaggleEventVis & ev,
  // OUTPUTS:
  Hypothesis & hyp, // on exit, this should be a hpothesis with a non-zero likelihood, so long as ptmiss is allowed to be mismeasured to zero
  unsigned int & attempts // possibly useful for debug
) {

  //const double sm = ev.sMu.m();  // hadronic tau mass (visible part)
  const double tm = ev.tMu.m();  // lepon mass from other tau

  const double chiMax // chiMax is largest possible mass of the system of two neutrinos from the leptoncially decaying tau
    = (tauMass - tm);
  const double chiSqMax = chiMax*chiMax;

  hyp.deltax = 0;
  hyp.deltay = 0;
  hyp.chiSq = chiSqMax;
  hyp.pXMiss = 0;
  hyp.pYMiss = 0;

  KaggleEventVis modifiedEvent = ev; // copy most of event
  modifiedEvent.pTMiss = CLHEP::Hep2Vector(hyp.pXMiss,hyp.pYMiss);  // overwrite pTMiss, setting it to (in this case) zero
  InvisSolns invisSolns;

  const unsigned int maxIt = 1000;
  bool foundGoodSolution = false;

  for (unsigned int i=0; i<maxIt; ++i) {

    // test start point:
    findPhaseSpaceSolns(
      // INPUTS:
      modifiedEvent, hyp.deltax, hyp.deltay, hyp.chiSq,
      // OUTPUTS:
      invisSolns);

    if (invisSolns.size()==4) {
      // We know the start point is good!
      foundGoodSolution = true;
      attempts = i+1; // might be useful for debug
      break;
    }

    // The start point was not good. :(
    // But mathematic notebook says that if we make chiSq small enough, we will eventually find a good point, so:
    hyp.chiSq *= 0.75;
  }

  if (!foundGoodSolution) {
    // shouldn't get here, so
    throw std::string("Bad logic, blame Chris");
  }

}

bool debugResoOn = false;

double propToLogLikelihood(const KaggleEventVis & obs, /* given */ const Hypothesis & hyp, const Parameters & params, const ExperimentalParameters & eParams) {
  // "Hypothesis hyp" contains the things we might change in the markov walk, like mom splittings.
  // "Parameters params" contains things we won't change in the markov walk, like the resonance mass that participates int the higgs (or Z) mass contraint.

  if (hyp.outOfRange()) {
    throw Lester::LogarithmicTools::LogOfZero();
  }

  // ***** First do some general preparation:
  double ans=0;
  const CLHEP::HepLorentzVector & visMom = obs.sMu + obs.tMu;

  // ***** TAKE INTO ACCOUNT THE PHASE SPACE, TAU-MASS AND RESONANCE MASS CONSTRAINTS  ****
  {
    KaggleEventVis modifiedEvent = obs;
    modifiedEvent.pTMiss = hyp.pTMiss();

    InvisSolns invisSolns;

    findPhaseSpaceSolns(
      // INPUTS:
      modifiedEvent, hyp.deltax, hyp.deltay, hyp.chiSq,
      // OUTPUTS:
      invisSolns);

    const unsigned int expectedNumberOfSolns = 4;
    if (invisSolns.size()!=expectedNumberOfSolns) {
      throw Lester::LogarithmicTools::LogOfZero();
    }
    {
      Lester::LogarithmicTools::IncrementalAdder adder;
      for(int i=0; i<expectedNumberOfSolns; ++i) {
        const InvisSoln & invisSoln = invisSolns[i];

        // Here, really just want to get (part1*part2*part3)/4 and add it to total, however part3 may be very small, so calculation needs to be done in log-space. And we can ignore the 4 as proportional is fine.
        const double part1 = (invisSoln->weightFromConstraints); // TAU-MASS CONSTRAINTS
        const double part2 = (invisSoln->weightFromFinalState);  // PHASE SPACE
        const double hypResonanceMass = ( invisSoln->pMu + invisSoln->qMu + visMom).m();

        if (debugResoOn) {
          std::cout << "RESO " << hypResonanceMass << std::endl;
        }
        double propToLogOfPart3;
        if (params.cauchy) {
          propToLogOfPart3 = -log(3.14159*(1.0+leicesterSquare((hypResonanceMass - params.resonanceMass)/params.resonanceMassConstraintWidth))); // RESONANCE MASS CONSTRAINT
        } else {
          const double thing = -0.5*log(2.0*3.14159*leicesterSquare(params.resonanceMassConstraintWidth)); // normalising constant
          propToLogOfPart3 = thing-0.5*leicesterSquare((hypResonanceMass - params.resonanceMass)/params.resonanceMassConstraintWidth); // RESONANCE MASS CONSTRAINT
        }
        const double propToLogOfPartsProduct = log(part1*part2) + propToLogOfPart3; // PUT THEM ALL TOGETHER
        adder.addNumberWhoseLogIs(propToLogOfPartsProduct); // AVERAGE OVER THE FOUR SOLNS
      }
      ans += adder.logOfTotal(); // will throw if total is too small // RECORD TEH LOT
    }
  }

  // ***** TAKE INTO ACCOUNT THE PTMISS MIS-MEASUREMENT CONSTRAINT ****
  {

    // The ptmiss likelihood is going to be of the form   P = 1/sqrt(2 pi sigma^2) exp (-(deltaPTMiss)^2/(2 sigma^2)
    // but we don't need to worry about the intital factors, since we will not change the sigma with time in any important function
    // We return propToLogLikelihoods, so:
    const double deltaPTMiss = (obs.pTMiss - CLHEP::Hep2Vector(hyp.pXMiss, hyp.pYMiss)).mag();
    ans -= 0.5 * leicesterSquare(deltaPTMiss/eParams.pTMissConstraintWidth);
    //std::cout << "KOO " << deltaPTMiss/eParams.pTMissConstraintWidth << " " << CLHEP::Hep2Vector(hyp.pXMiss, hyp.pYMiss) << " " << obs.pTMiss << " " << deltaPTMiss << " " << eParams.pTMissConstraintWidth<< std::endl;
  }

  // ***** WE ARE DONE  ****

  return ans;
}

class MySimpleSampler : public Lester::MetropolisSampler<Hypothesis> {
 public:
  MySimpleSampler(const Hypothesis & initialHypothesis, const KaggleEventVis & vis, const Parameters & params, const ExperimentalParameters & eParams) : Lester::MetropolisSampler<Hypothesis>(initialHypothesis), vis(vis), params(params), eParams(eParams) {
  }
 public:
  virtual double logTargetDistribution(const Hypothesis & hyp) const {
    return propToLogLikelihood(vis, /*given*/ hyp, params, eParams);  // This will throw if the startPoint is actually not good -- which shouldn't be the case!
  }
  virtual Hypothesis suggestNewPoint() const {
    Hypothesis ans = **this; // Copy current hypothesis ...
    const double sc = 2; // scaling factor for all params
    ans.deltax += CLHEP::RandGauss::shoot(0, 0.1*sc);
    ans.deltay += CLHEP::RandGauss::shoot(0, 0.1*sc);
    ans.chiSq += CLHEP::RandGauss::shoot(0, 0.1*sc);
    ans.pXMiss += CLHEP::RandGauss::shoot(0, 0.1*sc);
    ans.pYMiss += CLHEP::RandGauss::shoot(0, 0.1*sc);
    return ans;
  }
  virtual double logProbOfSuggestingFirstGivenLast(const Hypothesis & a, const Hypothesis & b) const {
    return 0; // since proposal distribution is symmetric
  }
  const KaggleEventVis & vis;
  const Parameters & params;
  const ExperimentalParameters & eParams;
};


class MyNextSampler : public Lester::MetropolisSampler<Hypothesis> {
 public:
  MyNextSampler(const Hypothesis & initialHypothesis, const KaggleEventVis & vis,
                const Parameters & sParams, // signal params
                const Parameters & bParams, // background params
                const ExperimentalParameters & eParams // experimental params
               ) : Lester::MetropolisSampler<Hypothesis>(initialHypothesis), vis(vis), sParams(sParams), bParams(bParams), eParams(eParams) {
  }
 public:
  virtual double logTargetDistribution(const Hypothesis & hyp) const {
    double propToLogOfSignalProb;
    double propToLogOfBackgrProb;
    bool sThrew = false;
    bool bThrew = false;
    try {
      propToLogOfSignalProb = propToLogLikelihood(vis, /*given*/ hyp, sParams, eParams);  // This will throw if the startPoint is actually not good -- which shouldn't be the case!
    } catch (...) {
      sThrew = true;
    }
    try {
      propToLogOfBackgrProb = propToLogLikelihood(vis, /*given*/ hyp, bParams, eParams);  // This will throw if the startPoint is actually not good -- which shouldn't be the case!
    } catch (...) {
      bThrew = true;
    }
    if (sThrew && !bThrew) {
      return propToLogOfBackgrProb;
    }
    if (bThrew && !sThrew) {
      return propToLogOfSignalProb;
    }
    if (sThrew && bThrew) {
      throw Lester::LogarithmicTools::LogOfZero();
    }
    assert(!sThrew && !bThrew);
    // Need to return the log of the sum of the probs to which propToLogOfSignalProb and propToLogOfBackgrProb refer:
    Lester::LogarithmicTools::IncrementalAdder adder;
    adder.addNumberWhoseLogIs(propToLogOfSignalProb);
    adder.addNumberWhoseLogIs(propToLogOfBackgrProb);
    return adder.logOfTotal();
  }
  virtual Hypothesis suggestNewPoint() const {
    Hypothesis ans = **this; // Copy current hypothesis ...
    const double sc = 2; // scaling factor for all params
    ans.deltax += CLHEP::RandGauss::shoot(0, 0.1*sc);
    ans.deltay += CLHEP::RandGauss::shoot(0, 0.1*sc);
    ans.chiSq += CLHEP::RandGauss::shoot(0, 0.1*sc);
    ans.pXMiss += CLHEP::RandGauss::shoot(0, 0.1*sc);
    ans.pYMiss += CLHEP::RandGauss::shoot(0, 0.1*sc);
    return ans;
  }
  virtual double logProbOfSuggestingFirstGivenLast(const Hypothesis & a, const Hypothesis & b) const {
    return 0; // since proposal distribution is symmetric
  }
  const KaggleEventVis & vis;
  const Parameters & sParams;
  const Parameters & bParams;
  const ExperimentalParameters & eParams;
};


#endif



























