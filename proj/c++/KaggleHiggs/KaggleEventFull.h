#ifndef LESTERKAGGLEEVENTFULLH
#define LESTERKAGGLEEVENTFULLH

class KaggleEventFull; // fwd dec

#include <ostream>
#include "KaggleHiggs/KaggleEventVis.h"
#include "KaggleHiggs/KaggleEventInvis.h"

class KaggleEventFull {
public:
	KaggleEventInvis invis;
	KaggleEventVis vis;
};

std::ostream & operator<<(std::ostream & os, const KaggleEventFull & event) {
   return os << "KaggleEventFull["
	     << event.vis <<", "
	     << event.invis <<"]";
}

#endif
