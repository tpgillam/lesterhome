#ifndef LESTERHYPOTHESISH
#define LESTERHYPOTHESISH

#include "CLHEP/Vector/TwoVector.h"

class Hypothesis {
 public:
  double deltax; // a parameter splitting pxmiss between the two sets of neutrino systems
  double deltay; // a parameter splitting pymiss between the two sets of neutrino systems
  double chiSq; // mass of the di-neutrino system from the leptonically decaying tay
  double pXMiss; // what the truth pXMiss might have been, as opposed to what was observed
  double pYMiss; // what the truth pYMiss might have been, as opposed to what was observed
  const CLHEP::Hep2Vector pTMiss() const {
      return CLHEP::Hep2Vector(pXMiss,pYMiss);
  }
  bool outOfRange() const {
    return chiSq<0;
  }
};

inline std::ostream & operator<<(std::ostream & os, const Hypothesis & hyp) {
  return os
         << "Hypothesis["
         << "dx= " << hyp.deltax
         << ", dy= " << hyp.deltay
         << ", chiSq= " << hyp.chiSq
         << ", pXMiss= " << hyp.pXMiss
         << ", pYMiss= " << hyp.pYMiss
         << " ]";

}

#endif


