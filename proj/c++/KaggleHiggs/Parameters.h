#ifndef LESTERPARAMETERSH
#define LESTERPARAMETERSH

#include <ostream>

class Parameters {
 public:
  double resonanceMass;
  double resonanceMassConstraintWidth; // that's the width used in the constraint, not the actual width of the resonance
  bool cauchy;
};

inline std::ostream & operator<<(std::ostream & os, const Parameters & params) {
  return os
         << "Parameters["
         << "resonanceMass=" << params.resonanceMass
         << ", resonanceMassConstraintWidth=" << params.resonanceMassConstraintWidth
         << "]";

}

#endif
