const double kDiscriminant = 
leicesterSquare(chiSq) + leicesterSquare(mTauSq) - 
  4.*(leicesterSquare(deltax - pXMissOnTwo) + 
     leicesterSquare(deltay - pYMissOnTwo))*leicesterSquare(te) + 
  2.*(chiSq + 2.*deltax*tx - 2.*pXMissOnTwo*tx + 2.*deltay*ty - 
     2.*pYMissOnTwo*ty)*leicesterSquare(tm) - 
  2.*mTauSq*(chiSq + 2.*deltax*tx - 2.*pXMissOnTwo*tx + 2.*deltay*ty - 
     2.*pYMissOnTwo*ty + leicesterSquare(tm)) - 
  4.*chiSq*(tx*(-deltax + pXMissOnTwo + tx) + 
     ty*(-deltay + pYMissOnTwo + ty) + leicesterSquare(tm)) + 
  4.*(leicesterSquare(deltax*tx - pXMissOnTwo*tx + 
       (deltay - pYMissOnTwo)*ty) + 
     (leicesterSquare(deltax - pXMissOnTwo) + 
        leicesterSquare(deltay - pYMissOnTwo))*leicesterSquare(tz)) + 
  pow(tm,4.)
;
