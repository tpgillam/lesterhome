#ifndef LESTERWEIGHTSH
#define LESTERWEIGHTSH

class Weights {
 public:
  Weights(const double weightFromConstraints,
          const double weightFromFinalState) :
    weightFromConstraints(weightFromConstraints),
    weightFromFinalState(weightFromFinalState) {
  }
  double weightFromConstraints;
  double weightFromFinalState;
 private:
  Weights(); // deliberately not implemented
};

inline std::ostream & operator<<(std::ostream & os, const Weights & event) {
  return os
         << "Weights[weightFromConstraints="<<event.weightFromConstraints
         <<", weightFromFinalState="<<event.weightFromFinalState <<"]";
}

#endif






