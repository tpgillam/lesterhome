#ifndef LESTERTYPESH
#define LESTERTYPESH

#include "boost/shared_ptr.hpp"
#include <vector>

class KaggleEventInvisWithWeights;
typedef boost::shared_ptr<KaggleEventInvisWithWeights> InvisSoln;
typedef std::vector<InvisSoln> InvisSolns;
std::ostream & operator<<(std::ostream & os, const InvisSolns & solns);

#endif

