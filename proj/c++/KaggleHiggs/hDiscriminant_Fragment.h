const double hDiscriminant = 
-4.*(leicesterSquare(deltax + pXMissOnTwo) + 
     leicesterSquare(deltay + pYMissOnTwo))*leicesterSquare(se) + 
  4.*(leicesterSquare(deltax + pXMissOnTwo) + 
     leicesterSquare(deltay + pYMissOnTwo))*leicesterSquare(sz) + 
  leicesterSquare(mTauSq + 2.*(deltax + pXMissOnTwo)*sx + 
    2.*(deltay + pYMissOnTwo)*sy - pow(sm,2.))
;
