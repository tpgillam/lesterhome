#ifndef LESTERKAGGLEEVENTVISH
#define LESTERKAGGLEEVENTVISH

class KaggleEventVis; // fwd dec

#include <ostream>
#include "CLHEP/Vector/LorentzVector.h"
#include "CLHEP/Vector/TwoVector.h"

class KaggleEventVis {
public:
  CLHEP::HepLorentzVector sMu; // visible bits of hadronically decaying tau
  CLHEP::HepLorentzVector tMu; // visible lepton in decay of other tau
  CLHEP::Hep2Vector pTMiss;
public:
  // Canoncial form has pTMiss along positive X-axis
  /*
  const KaggleEventVis getCanonicalForm() const {
    KaggleEventVis ans;
    const double theta = pTMiss.phi();
    ans.pTMiss = CLHEP::Hep2Vector(pTMiss.mag(),0);
    ans.sMu = CLHEP::rotationZOf(sMu,-theta);
    ans.tMu = CLHEP::rotationZOf(tMu,-theta);
    return ans;
  }*/
};

inline std::ostream & operator<<(std::ostream & os, const KaggleEventVis & event) {
   return os << "KaggleEventVis[hadronicTauVis(sMu)="<<event.sMu<<", lep(tMu)=" << event.tMu << ", pTMiss=" << event.pTMiss << "]";
}

inline std::istream & operator>>(std::istream & is, KaggleEventVis & k) {
  double e,px,py,pz;
  is >> e >> px >> py >> pz;
  k.sMu = CLHEP::HepLorentzVector(CLHEP::Tcomponent(e), px, py, pz);
  is >> e >> px >> py >> pz;
  k.tMu = CLHEP::HepLorentzVector(CLHEP::Tcomponent(e), px, py, pz);
  is >> px >> py;
  k.pTMiss = CLHEP::Hep2Vector(px, py);
  return is;
}


#endif
