#ifndef LESTERKAGGLEEVENTINVISWITHWEIGHTSH
#define LESTERKAGGLEEVENTINVISWITHWEIGHTSH

class KaggleEventInvisWithWeights; // fwd dec

#include <ostream>
#include "KaggleEventInvis.h"
#include "Weights.h"

class KaggleEventInvisWithWeights : public KaggleEventInvis, public Weights {
public:
  KaggleEventInvisWithWeights(const KaggleEventInvis & ev, const Weights & w) : KaggleEventInvis(ev), Weights(w) {
  }
private:
  KaggleEventInvisWithWeights(); // deliberately not implemented
};

inline std::ostream & operator<<(std::ostream & os, const KaggleEventInvisWithWeights & event) {
   return os << "KaggleEventInvisWithWeights["
     <<static_cast<const KaggleEventInvis &>(event)<<", "
     <<static_cast<const Weights &>(event)<<"]";
}

#endif
