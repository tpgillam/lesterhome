-2.*tz + (2.*k*te)/
   sqrt(chiSq + leicesterSquare(k) + leicesterSquare(deltax - pXMissOnTwo) + 
     leicesterSquare(deltay - pYMissOnTwo))
; // Quantity fixedDTauMassSq2ByDK calculated by 20140821a-KaggleHiggs.nb
