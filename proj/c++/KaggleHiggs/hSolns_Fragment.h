const double hSoln1 = 
(sz*(mTauSq + 2.*(deltax + pXMissOnTwo)*sx + 
       2.*(deltay + pYMissOnTwo)*sy - leicesterSquare(sm)) - 
    se*sqrt(hDiscriminant))/(2.*(se - sz)*(se + sz))
;
const double hSoln2 = 
(sz*(mTauSq + 2.*(deltax + pXMissOnTwo)*sx + 
       2.*(deltay + pYMissOnTwo)*sy - leicesterSquare(sm)) + 
    se*sqrt(hDiscriminant))/(2.*(se - sz)*(se + sz))
;
