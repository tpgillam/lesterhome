#ifndef LESTEREXPERIMENTALPARAMETERSH
#define LESTEREXPERIMENTALPARAMETERSH

#include <ostream>

class ExperimentalParameters {
 public:
  double pTMissConstraintWidth;
};

inline std::ostream & operator<<(std::ostream & os, const ExperimentalParameters & params) {
  return os
         << "ExperimentalParameters["
         << "pTMissConstraintWidth=" << params.pTMissConstraintWidth
         << "]";

}

#endif
